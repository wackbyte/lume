{
  description = "lume";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, flake-utils, fenix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        lib = pkgs.lib;

        rustToolchain = fenix.packages.${system}.minimal;
        rustPlatform = pkgs.makeRustPlatform {
          inherit (rustToolchain) cargo rustc;
        };
      in
      {
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            rustToolchain.cargo
            rustToolchain.rustc
            pkgs.nixpkgs-fmt
          ];
        };
      }
    );
}
