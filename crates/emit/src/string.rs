//! String literal formatting.

/// The quote used in a string literal.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Quote {
    /// Single quote: `'`.
    Single,
    /// Double quote: `"`.
    Double,
}

impl Quote {
    /// Is it [`Single`](Self::Single)?
    pub const fn is_single(self) -> bool {
        matches!(self, Self::Single)
    }

    /// Is it [`Double`](Self::Double)?
    pub const fn is_double(self) -> bool {
        matches!(self, Self::Double)
    }

    /// Get the character associated with the quote.
    pub const fn symbol(self) -> char {
        match self {
            Self::Single => '\'',
            Self::Double => '"',
        }
    }
}

impl Default for Quote {
    fn default() -> Self {
        Self::Double
    }
}

impl From<Quote> for char {
    fn from(quote: Quote) -> Self {
        quote.symbol()
    }
}

/// Escape a string with Lua's escape codes.
///
/// These include:
///
/// - `\a`: Bell
/// - `\b`: Backspace
/// - `\f`: Form feed
/// - `\n`: New line
/// - `\r`: Carriage return
/// - `\t`: Horizontal tab
/// - `\v`: Vertical tab
/// - `\\`: Backslash
/// - `\'` or `\"`: Quotes
/// - As well as `\XXX`, representing the decimal digits of an ASCII character.
///
/// Quotes are escaped based on the given [`Quote`].
/// Surrounding quotes come separately.
pub fn escape(string: &str, quote: Quote) -> String {
    let quote = quote.symbol();

    let mut buffer = String::with_capacity(string.len());
    let mut chars = string.chars().peekable();
    while let Some(c) = chars.next() {
        match c {
            // bell
            '\x07' => {
                buffer.push_str("\\a");
            }
            // backspace
            '\x08' => {
                buffer.push_str("\\b");
            }
            // form feed
            '\x0C' => {
                buffer.push_str("\\f");
            }
            // new line
            '\n' => {
                buffer.push_str("\\n");
            }
            // carriage return
            '\r' => {
                buffer.push_str("\\r");
            }
            // horizontal tab
            '\t' => {
                buffer.push_str("\\t");
            }
            // vertical tab
            '\x0B' => {
                buffer.push_str("\\v");
            }
            // other control characters
            _ if c.is_ascii_control() => {
                // should only ever be up to 3 characters (always valid)
                let number = (c as u8).to_string();
                // trailing digits must be handled specially (Lua is dumb)
                let next_is_digit = matches!(chars.peek(), Some('0'..='9'));

                buffer.push('\\');
                if next_is_digit {
                    match number.len() {
                        1 => {
                            buffer.push_str("00");
                        }
                        2 => {
                            buffer.push('0');
                        }
                        _ => {}
                    }
                }
                buffer.push_str(&number);
            }
            // backslash
            '\\' => {
                buffer.push_str("\\\\");
            }
            // quote
            _ if c == quote => {
                buffer.push('\\');
                buffer.push(quote);
            }
            // other characters
            _ => {
                buffer.push(c);
            }
        }
    }
    buffer
}

/// Escape a string with [single quotes](Quote::Single).
///
/// See [`escape`].
pub fn escape_single(string: &str) -> String {
    escape(string, Quote::Single)
}

/// Escape a string with [double quotes](Quote::Double).
///
/// See [`escape`].
pub fn escape_double(string: &str) -> String {
    escape(string, Quote::Double)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn identity() {
        assert_eq!(escape_single("able lua (ablua)"), "able lua (ablua)");
        assert_eq!(escape_double("  foo bar 123  "), "  foo bar 123  ");
    }

    #[test]
    fn quotes() {
        assert_eq!(escape_single("'"), "\\'");
        assert_eq!(escape_double("'"), "'");
        assert_eq!(escape_single("\""), "\"");
        assert_eq!(escape_double("\""), "\\\"");
    }

    #[test]
    fn numbers() {
        // null (1 digit)
        assert_eq!(escape_double("\x00"), "\\0");
        assert_eq!(escape_double("\x00a"), "\\0a");
        assert_eq!(escape_double("\x000"), "\\0000");

        // escape (2 digits)
        assert_eq!(escape_double("\x1B"), "\\27");
        assert_eq!(escape_double("\x1Ba"), "\\27a");
        assert_eq!(escape_double("\x1B0"), "\\0270");

        // delete (3 digits)
        assert_eq!(escape_double("\x7F"), "\\127");
        assert_eq!(escape_double("\x7Fa"), "\\127a");
        assert_eq!(escape_double("\x7F0"), "\\1270");
    }

    #[test]
    fn exhaustive() {
        assert_eq!(
            escape_double("\x07\x08\x0C\n\r\t\x0B\x00\\\" "),
            "\\a\\b\\f\\n\\r\\t\\v\\0\\\\\\\" "
        );
    }
}
