//! Whitespace management.

use lume_ast::*;

/// A trait for managing whitespace.
pub trait Whitespace {
    /// If whitespace is required before.
    fn prefix_with_whitespace(&self) -> bool;

    /// If whitespace is required after.
    fn suffix_with_whitespace(&self) -> bool;
}

impl<S> Whitespace for Block<S> {
    fn prefix_with_whitespace(&self) -> bool {
        if let Some(first) = self.body.first() {
            first.prefix_with_whitespace()
        } else if let Some(result) = &self.result {
            result.prefix_with_whitespace()
        } else {
            false
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        if let Some(result) = &self.result {
            result.suffix_with_whitespace()
        } else if let Some(last) = self.body.last() {
            last.suffix_with_whitespace()
        } else {
            false
        }
    }
}

impl<S> Whitespace for Return<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Returns always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        if let Some(last) = self.values.last() {
            last.suffix_with_whitespace()
        } else {
            // Empty returns end with a keyword.
            true
        }
    }
}

impl<S> Whitespace for Group<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Groups always start with a parenthesis.
        false
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Groups always end with a parenthesis.
        false
    }
}

impl<S> Whitespace for Prefix<S> {
    fn prefix_with_whitespace(&self) -> bool {
        match self {
            Self::Variable(variable) => variable.prefix_with_whitespace(),
            Self::Call(call) => call.prefix_with_whitespace(),
            Self::Group(group) => group.prefix_with_whitespace(),
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        match self {
            Self::Variable(variable) => variable.suffix_with_whitespace(),
            Self::Call(call) => call.suffix_with_whitespace(),
            Self::Group(group) => group.suffix_with_whitespace(),
        }
    }
}

impl<S> Whitespace for Index<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.prefix.prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Indices always end with a bracket.
        false
    }
}

impl<S> Whitespace for Access<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.prefix.prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Accesses always end with an identifier.
        true
    }
}

impl<S> Whitespace for Variable<S> {
    fn prefix_with_whitespace(&self) -> bool {
        match self {
            Self::Name(_) => true,
            Self::Index(index) => index.prefix_with_whitespace(),
            Self::Access(access) => access.prefix_with_whitespace(),
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        match self {
            Self::Name(_) => true,
            Self::Index(index) => index.suffix_with_whitespace(),
            Self::Access(access) => access.suffix_with_whitespace(),
        }
    }
}

impl Whitespace for Attribute {
    fn prefix_with_whitespace(&self) -> bool {
        // Attributes always start with an arrow.
        false
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Attributes always end with an arrow.
        // However, care must be taken to ensure that it isn't
        // immediately followed by an equal sign, `=`, as Lua will
        // tokenize that as a greater-than or equal-to symbol, `>=`.
        false
    }
}

impl<S> Whitespace for LocalName<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Local names always start with an identifier.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        if let Some(attribute) = self.attribute {
            attribute.suffix_with_whitespace()
        } else {
            true
        }
    }
}

impl<S> Whitespace for Local<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Locals always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        if let Some(last) = self.values.last() {
            last.suffix_with_whitespace()
        } else {
            self.names.last().suffix_with_whitespace()
        }
    }
}

impl<S> Whitespace for Assign<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.variables.first().prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.values.last().suffix_with_whitespace()
    }
}

impl<S> Whitespace for FunctionName<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Function names always start with an identifier.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Function names always end with an identifier.
        true
    }
}

impl<S> Whitespace for FunctionBody<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Function bodies always begin with a parenthesis.
        false
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Function bodies always end with a keyword.
        true
    }
}

impl<S> Whitespace for Function<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Functions always begin with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.body.suffix_with_whitespace()
    }
}

impl<S> Whitespace for LocalFunction<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Local functions always begin with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.body.suffix_with_whitespace()
    }
}

impl<S> Whitespace for InlineFunction<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Inline functions always begin with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.body.suffix_with_whitespace()
    }
}

impl<S> Whitespace for Call<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.prefix.prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Calls always end with a parenthesis.
        false
    }
}

impl<S> Whitespace for Label<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Labels always start with a colon.
        false
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Labels always end with a colon.
        false
    }
}

impl<S> Whitespace for Goto<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Gotos always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Gotos always end with an identifier.
        true
    }
}

impl<S> Whitespace for Do<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Dos always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Dos always end with a keyword.
        true
    }
}

impl<S> Whitespace for If<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Ifs always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Ifs always end with a keyword.
        true
    }
}

impl<S> Whitespace for While<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Whiles always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Whiles always end with a keyword.
        true
    }
}

impl<S> Whitespace for Repeat<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Repeats always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Repeats always end with a keyword.
        true
    }
}

impl<S> Whitespace for For<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // Fors always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // Fors always end with a keyword.
        true
    }
}

impl<S> Whitespace for ForIn<S> {
    fn prefix_with_whitespace(&self) -> bool {
        // For ins always start with a keyword.
        true
    }

    fn suffix_with_whitespace(&self) -> bool {
        // For ins always end with a keyword.
        true
    }
}

impl<S> Whitespace for Statement<S> {
    fn prefix_with_whitespace(&self) -> bool {
        match self {
            Self::Local(local) => local.prefix_with_whitespace(),
            Self::Assign(assign) => assign.prefix_with_whitespace(),
            Self::Function(function) => function.prefix_with_whitespace(),
            Self::LocalFunction(local_function) => local_function.prefix_with_whitespace(),
            Self::Call(call) => call.prefix_with_whitespace(),
            Self::Do(do_) => do_.prefix_with_whitespace(),
            Self::If(if_) => if_.prefix_with_whitespace(),
            Self::Label(label) => label.prefix_with_whitespace(),
            Self::Goto(goto) => goto.prefix_with_whitespace(),
            Self::While(while_) => while_.prefix_with_whitespace(),
            Self::Repeat(repeat) => repeat.prefix_with_whitespace(),
            Self::For(for_) => for_.prefix_with_whitespace(),
            Self::ForIn(for_in) => for_in.prefix_with_whitespace(),
            Self::Break => true,
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        match self {
            Self::Local(local) => local.suffix_with_whitespace(),
            Self::Assign(assign) => assign.suffix_with_whitespace(),
            Self::Function(function) => function.suffix_with_whitespace(),
            Self::LocalFunction(local_function) => local_function.suffix_with_whitespace(),
            Self::Call(call) => call.suffix_with_whitespace(),
            Self::Do(do_) => do_.suffix_with_whitespace(),
            Self::If(if_) => if_.suffix_with_whitespace(),
            Self::Label(label) => label.suffix_with_whitespace(),
            Self::Goto(goto) => goto.suffix_with_whitespace(),
            Self::While(while_) => while_.suffix_with_whitespace(),
            Self::Repeat(repeat) => repeat.suffix_with_whitespace(),
            Self::For(for_) => for_.suffix_with_whitespace(),
            Self::ForIn(for_in) => for_in.suffix_with_whitespace(),
            Self::Break => true,
        }
    }
}

impl<S> Whitespace for Field<S> {
    fn prefix_with_whitespace(&self) -> bool {
        match self {
            Self::Name(_, _) => true,
            Self::Key(_, _) => false,
            Self::Item(value) => value.prefix_with_whitespace(),
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        match self {
            Self::Name(_, value) => value.suffix_with_whitespace(),
            Self::Key(_, value) => value.suffix_with_whitespace(),
            Self::Item(value) => value.suffix_with_whitespace(),
        }
    }
}

impl Whitespace for UnaryOp {
    fn prefix_with_whitespace(&self) -> bool {
        self.is_word()
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.is_word()
    }
}

impl<S> Whitespace for Unary<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.op.prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.value.suffix_with_whitespace()
    }
}

impl Whitespace for BinaryOp {
    fn prefix_with_whitespace(&self) -> bool {
        self.is_word()
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.is_word()
    }
}

impl<S> Whitespace for Binary<S> {
    fn prefix_with_whitespace(&self) -> bool {
        self.left.prefix_with_whitespace()
    }

    fn suffix_with_whitespace(&self) -> bool {
        self.right.suffix_with_whitespace()
    }
}

impl<S> Whitespace for Expression<S> {
    fn prefix_with_whitespace(&self) -> bool {
        match self {
            Self::Nil | Self::Boolean(_) | Self::Number(_) => true,
            Self::String(_) | Self::Table(_) | Self::Ellipsis => false,
            Self::Function(function) => function.prefix_with_whitespace(),
            Self::Unary(unary) => unary.prefix_with_whitespace(),
            Self::Binary(binary) => binary.prefix_with_whitespace(),
            Self::Prefix(prefix) => prefix.prefix_with_whitespace(),
        }
    }

    fn suffix_with_whitespace(&self) -> bool {
        match self {
            Self::Nil | Self::Boolean(_) | Self::Number(_) => true,
            Self::String(_) | Self::Table(_) | Self::Ellipsis => false,
            Self::Function(function) => function.suffix_with_whitespace(),
            Self::Unary(unary) => unary.suffix_with_whitespace(),
            Self::Binary(binary) => binary.suffix_with_whitespace(),
            Self::Prefix(prefix) => prefix.suffix_with_whitespace(),
        }
    }
}
