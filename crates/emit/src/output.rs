//! Outputs for emission.

/// An output.
pub trait Output {
    /// Push a character to the output.
    fn push_char(&mut self, char: char);

    /// Extend the output with a string.
    fn push_str(&mut self, str: &str);
}

impl Output for String {
    fn push_char(&mut self, char: char) {
        self.push(char);
    }

    fn push_str(&mut self, str: &str) {
        self.push_str(str);
    }
}
