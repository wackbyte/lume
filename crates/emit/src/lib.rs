//! Common utilities for formatting and output.

pub mod output;
pub mod string;
pub mod whitespace;

pub use self::{
    output::Output,
    string::{escape, escape_double, escape_single, Quote},
    whitespace::Whitespace,
};
