//! Lua's abstract syntax tree.

use non_empty_vec::NonEmpty;

/// A block.
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub struct Block<S> {
    /// A series of statements.
    pub body: Vec<Statement<S>>,
    /// A block may be terminated with a `return` statement.
    pub result: Option<Return<S>>,
}

impl<S> Block<S> {
    pub const fn new() -> Self {
        Self {
            body: Vec::new(),
            result: None,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.body.is_empty() && self.result.is_none()
    }
}

/// A return statement.
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub struct Return<S> {
    pub values: Vec<Expression<S>>,
}

impl<S> Return<S> {
    pub const fn new() -> Self {
        Self { values: Vec::new() }
    }
}

/// A parenthesized expression.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Group<S> {
    /// The inner expression.
    pub inner: Expression<S>,
}

/// The prefix of a function call (although it is also used in other positions).
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Prefix<S> {
    /// See [`Variable`].
    Variable(Box<Variable<S>>),
    /// See [`Call`].
    Call(Box<Call<S>>),
    /// See [`Group`].
    Group(Box<Group<S>>),
}

impl<S> From<Variable<S>> for Prefix<S> {
    fn from(variable: Variable<S>) -> Self {
        Self::Variable(Box::new(variable))
    }
}

impl<S> From<Call<S>> for Prefix<S> {
    fn from(call: Call<S>) -> Self {
        Self::Call(Box::new(call))
    }
}

impl<S> From<Group<S>> for Prefix<S> {
    fn from(group: Group<S>) -> Self {
        Self::Group(Box::new(group))
    }
}

/// An index.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Index<S> {
    /// The value.
    pub prefix: Prefix<S>,
    /// The key.
    pub key: Expression<S>,
}

/// An access.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Access<S> {
    /// The value.
    pub prefix: Prefix<S>,
    /// The key.
    pub key: S,
}

/// A variable.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Variable<S> {
    /// A name.
    Name(S),
    /// See [`Index`].
    Index(Index<S>),
    /// See [`Access`].
    Access(Access<S>),
}

impl<S> From<S> for Variable<S> {
    fn from(name: S) -> Self {
        Self::Name(name)
    }
}

impl<S> From<Index<S>> for Variable<S> {
    fn from(index: Index<S>) -> Self {
        Self::Index(index)
    }
}

impl<S> From<Access<S>> for Variable<S> {
    fn from(access: Access<S>) -> Self {
        Self::Access(access)
    }
}

/// An attribute of a local.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum Attribute {
    /// `<const>`
    Const,
    /// `<close>`
    Close,
}

impl Attribute {
    /// The name associated with the attribute.
    pub const fn name(self) -> &'static str {
        match self {
            Self::Const => "const",
            Self::Close => "close",
        }
    }

    /// Is it [`<const>`](Self::Const)?
    pub const fn is_const(self) -> bool {
        matches!(self, Self::Const)
    }

    /// Is it [`<close>`](Self::Close)?
    pub const fn is_close(self) -> bool {
        matches!(self, Self::Close)
    }
}

/// The name of a local and its optional attribute.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct LocalName<S> {
    /// The name.
    pub name: S,
    /// The attribute.
    pub attribute: Option<Attribute>,
}

/// A local definition.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Local<S> {
    /// The names of the locals.
    pub names: NonEmpty<LocalName<S>>,
    /// The values, if any.
    pub values: Vec<Expression<S>>,
}

/// A variable assignment.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Assign<S> {
    pub variables: NonEmpty<Variable<S>>,
    pub values: NonEmpty<Expression<S>>,
}

/// The name of a function in its definition.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct FunctionName<S> {
    /// The base name.
    pub name: S,
    /// Keys into the base.
    pub keys: Vec<S>,
    /// The method.
    pub method: Option<S>,
}

/// The body of a function.
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub struct FunctionBody<S> {
    /// The function's named parameters.
    pub parameters: Vec<S>,
    /// If the function accepts varargs.
    pub ellipsis: bool,
    /// The block.
    pub block: Block<S>,
}

impl<S> FunctionBody<S> {
    pub const fn new() -> Self {
        Self {
            parameters: Vec::new(),
            ellipsis: false,
            block: Block::new(),
        }
    }
}

/// A function definition statement.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Function<S> {
    /// See [`FunctionName`].
    pub name: FunctionName<S>,
    /// See [`FunctionBody`].
    pub body: FunctionBody<S>,
}

/// A local function definition statement.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct LocalFunction<S> {
    /// The name of the local.
    pub name: S,
    /// See [`FunctionBody`].
    pub body: FunctionBody<S>,
}

/// An inline function.
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub struct InlineFunction<S> {
    /// See [`FunctionBody`].
    pub body: FunctionBody<S>,
}

impl<S> From<FunctionBody<S>> for InlineFunction<S> {
    fn from(body: FunctionBody<S>) -> Self {
        Self { body }
    }
}

/// A call.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Call<S> {
    pub prefix: Prefix<S>,
    pub method: Option<S>,
    pub arguments: Vec<Expression<S>>,
}

/// A label definition.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct Label<S> {
    /// The name of the label.
    pub name: S,
}

/// A goto statement.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct Goto<S> {
    /// The name of the label.
    pub name: S,
}

/// A block.
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub struct Do<S> {
    /// The inner block.
    pub block: Block<S>,
}

/// A conditional statement.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct If<S> {
    /// The condition.
    pub condition: Expression<S>,
    /// The block to execute if the condition is truthy.
    pub yes: Block<S>,
    /// A series of alternative conditions to test and blocks to execute.
    pub maybe: Vec<(Expression<S>, Block<S>)>,
    /// The fallback block.
    pub no: Option<Block<S>>,
}

/// A while loop.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct While<S> {
    /// The condition.
    pub condition: Expression<S>,
    /// The block.
    pub block: Block<S>,
}

/// The opposite of a while loop.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Repeat<S> {
    /// The block.
    pub block: Block<S>,
    /// The condition.
    pub condition: Expression<S>,
}

/// A for loop.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct For<S> {
    pub name: S,
    pub start: Expression<S>,
    pub end: Expression<S>,
    pub step: Option<Expression<S>>,
    pub block: Block<S>,
}

/// A for loop over an iterator.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct ForIn<S> {
    pub names: NonEmpty<S>,
    pub values: NonEmpty<Expression<S>>,
    pub block: Block<S>,
}

/// A statement.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Statement<S> {
    /// See [`Local`].
    Local(Local<S>),
    /// See [`Assign`].
    Assign(Assign<S>),

    /// See [`Function`].
    Function(Function<S>),
    /// See [`LocalFunction`].
    LocalFunction(LocalFunction<S>),
    /// See [`Call`].
    Call(Call<S>),

    /// See [`Do`].
    Do(Do<S>),
    /// See [`If`].
    If(If<S>),

    /// See [`Label`].
    Label(Label<S>),
    /// See [`Goto`].
    Goto(Goto<S>),

    /// See [`While`].
    While(While<S>),
    /// See [`Repeat`].
    Repeat(Repeat<S>),
    /// See [`For`].
    For(For<S>),
    /// See [`ForIn`].
    ForIn(ForIn<S>),

    /// A `break` statement.
    Break,
}

/// A table field.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Field<S> {
    /// A named field.
    Name(S, Expression<S>),
    /// A keyed field.
    Key(Expression<S>, Expression<S>),
    /// An array item.
    Item(Expression<S>),
}

/// A unary operator.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum UnaryOp {
    /// Negation: `-`.
    Neg,
    /// Bitwise NOT: `~`.
    BitNot,
    /// Logical NOT: `not`.
    Not,
    /// Length: `#`.
    Len,
}

impl UnaryOp {
    /// The symbol associated with the operator.
    pub const fn symbol(self) -> &'static str {
        match self {
            Self::Neg => "-",
            Self::BitNot => "~",
            Self::Not => "not",
            Self::Len => "#",
        }
    }

    /// Is it a keyword operator?
    pub const fn is_word(self) -> bool {
        matches!(self, Self::Not)
    }
}

/// A unary operation.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Unary<S> {
    /// The operator.
    pub op: UnaryOp,
    /// The operand.
    pub value: Expression<S>,
}

/// A binary operator.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum BinaryOp {
    /// Addition: `+`.
    Add,
    /// Subtraction: `-`.
    Sub,
    /// Multiplication: `*`.
    Mul,
    /// Division: `/`.
    Div,
    /// Integer division: `//`.
    IDiv,
    /// Modulus: `%`.
    Mod,
    /// Exponentiation: `^`.
    Pow,

    /// Bitwise AND: `&`.
    BitAnd,
    /// Bitwise OR: `|`.
    BitOr,
    /// Bitwise XOR: `~`.
    BitXor,
    /// Left shift: `<<`.
    Shl,
    /// Right shift: `>>`.
    Shr,

    /// Less than: `<`.
    Less,
    /// Less than or equal to: `<=`.
    LessEq,
    /// More than: `>`.
    More,
    /// More than or equal to: `>=`.
    MoreEq,
    /// Equal to: `==`.
    Eq,
    /// Not equal to: `~=`.
    NotEq,

    /// Short-circuiting logical AND: `and`.
    And,
    /// Short-circuiting logical OR: `or`.
    Or,

    /// Concatenation: `..`.
    Concat,
}

impl BinaryOp {
    /// The symbol associated with the operator.
    pub const fn symbol(self) -> &'static str {
        match self {
            Self::Add => "+",
            Self::Sub => "-",
            Self::Mul => "*",
            Self::Div => "/",
            Self::IDiv => "//",
            Self::Mod => "%",
            Self::Pow => "^",
            Self::BitAnd => "&",
            Self::BitOr => "|",
            Self::BitXor => "~",
            Self::Shl => "<<",
            Self::Shr => ">>",
            Self::Less => "<",
            Self::LessEq => "<=",
            Self::More => ">",
            Self::MoreEq => ">=",
            Self::Eq => "==",
            Self::NotEq => "~=",
            Self::And => "and",
            Self::Or => "or",
            Self::Concat => "..",
        }
    }

    /// Is it a keyword operator?
    pub const fn is_word(self) -> bool {
        matches!(self, Self::And | Self::Or)
    }
}

/// A binary operation.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Binary<S> {
    /// The operator.
    pub op: BinaryOp,
    /// The left-hand operand.
    pub left: Expression<S>,
    /// The right-hand operand.
    pub right: Expression<S>,
}

/// An expression.
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub enum Expression<S> {
    /// `nil`.
    Nil,
    /// A boolean literal.
    Boolean(bool),
    /// A number literal.
    Number(S),
    /// A string literal.
    String(S),
    /// A table literal.
    Table(Vec<Field<S>>),
    /// See [`InlineFunction`].
    Function(Box<InlineFunction<S>>),
    /// See [`Unary`].
    Unary(Box<Unary<S>>),
    /// See [`Binary`].
    Binary(Box<Binary<S>>),
    /// See [`Prefix`].
    Prefix(Prefix<S>),
    /// Varargs.
    Ellipsis,
}
