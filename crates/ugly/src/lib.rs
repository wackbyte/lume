//! Ugly output.

use {
    crate::utils::*,
    lume_ast::*,
    lume_emit::{escape_double, Output, Whitespace},
};

#[cfg(test)]
mod tests;

pub mod utils;

/// Emit ugly source code.
pub trait Ugly {
    /// Emit this node as ugly source code.
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output;
}

impl<S> Ugly for Block<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        let mut statements = self.body.iter();
        if let Some(first) = statements.next() {
            first.ugly(output);
            for statement in statements {
                output.push_char(';');
                statement.ugly(output);
            }
        }

        if let Some(result) = &self.result {
            if !self.body.is_empty() {
                output.push_char(';');
            }
            result.ugly(output);
        }
    }
}

impl<S> Ugly for Return<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("return");

        let mut values = self.values.iter();
        if let Some(first) = values.next() {
            ugly_with_prefix_padding(first, output);
            for value in values {
                output.push_char(',');
                value.ugly(output);
            }
        }
    }
}

impl<S> Ugly for Group<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_char('(');
        self.inner.ugly(output);
        output.push_char(')');
    }
}

impl<S> Ugly for Prefix<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        match self {
            Self::Variable(variable) => {
                variable.ugly(output);
            }
            Self::Call(call) => {
                call.ugly(output);
            }
            Self::Group(group) => {
                group.ugly(output);
            }
        }
    }
}

impl<S> Ugly for Index<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        self.prefix.ugly(output);
        output.push_char('[');
        self.key.ugly(output);
        output.push_char(']');
    }
}

impl<S> Ugly for Access<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        self.prefix.ugly(output);
        output.push_char('.');
        output.push_str(self.key.as_ref());
    }
}

impl<S> Ugly for Variable<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        match self {
            Self::Name(name) => {
                output.push_str(name.as_ref());
            }
            Self::Index(index) => {
                index.ugly(output);
            }
            Self::Access(access) => {
                access.ugly(output);
            }
        }
    }
}

impl Ugly for Attribute {
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_char('<');
        output.push_str(self.name());
        output.push_char('>');
    }
}

impl<S> Ugly for LocalName<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str(self.name.as_ref());
        if let Some(attribute) = self.attribute {
            attribute.ugly(output);
        }
    }
}

impl<S> Ugly for Local<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("local ");

        // Attributes must be specially handled to prevent
        // a `>=` token from forming.
        let mut ends_with_arrow = false;

        let mut names = self.names.iter();
        names.next().unwrap().ugly(output);
        for name in names {
            ends_with_arrow = name.attribute.is_some();

            output.push_char(',');
            name.ugly(output);
        }

        let mut values = self.values.iter();
        if let Some(first) = values.next() {
            if ends_with_arrow {
                output.push_char(' ');
            }
            output.push_char('=');

            first.ugly(output);
            for value in values {
                output.push_char(',');
                value.ugly(output);
            }
        }
    }
}

impl<S> Ugly for Assign<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        let mut variables = self.variables.iter();
        variables.next().unwrap().ugly(output);
        for variable in variables {
            output.push_char(',');
            variable.ugly(output);
        }

        output.push_char('=');

        let mut values = self.values.iter();
        values.next().unwrap().ugly(output);
        for value in values {
            output.push_char(',');
            value.ugly(output);
        }
    }
}

impl<S> Ugly for FunctionName<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str(self.name.as_ref());
        for key in &self.keys {
            output.push_char('.');
            output.push_str(key.as_ref());
        }
        if let Some(method) = &self.method {
            output.push_char(':');
            output.push_str(method.as_ref());
        }
    }
}

impl<S> Ugly for FunctionBody<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_char('(');
        let mut parameters = self.parameters.iter();
        if let Some(first) = parameters.next() {
            output.push_str(first.as_ref());
            for parameter in parameters {
                output.push_char(',');
                output.push_str(parameter.as_ref());
            }

            if self.ellipsis {
                output.push_char(',');
            }
        }
        if self.ellipsis {
            output.push_str("...");
        }
        output.push_char(')');

        ugly_with_suffix_padding(&self.block, output);
        output.push_str("end");
    }
}

impl<S> Ugly for Function<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("function ");
        self.name.ugly(output);
        self.body.ugly(output);
    }
}

impl<S> Ugly for LocalFunction<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("local function ");
        output.push_str(self.name.as_ref());
        self.body.ugly(output);
    }
}

impl<S> Ugly for InlineFunction<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("function");
        self.body.ugly(output);
    }
}

impl<S> Ugly for Call<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        self.prefix.ugly(output);

        if let Some(method) = &self.method {
            output.push_char(':');
            output.push_str(method.as_ref());
        }

        output.push_char('(');
        let mut arguments = self.arguments.iter();
        if let Some(first) = arguments.next() {
            first.ugly(output);
            for argument in arguments {
                output.push_char(',');
                argument.ugly(output);
            }
        }
        output.push_char(')');
    }
}

impl<S> Ugly for Label<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("::");
        output.push_str(self.name.as_ref());
        output.push_str("::");
    }
}

impl<S> Ugly for Goto<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("goto ");
        output.push_str(self.name.as_ref());
    }
}

impl<S> Ugly for Do<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("do");
        ugly_block_or_whitespace(&self.block, output);
        output.push_str("end");
    }
}

impl<S> Ugly for If<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("if");
        ugly_with_padding(&self.condition, output);
        output.push_str("then");
        ugly_block_or_whitespace(&self.yes, output);

        for (condition, block) in &self.maybe {
            output.push_str("elseif");
            ugly_with_padding(condition, output);
            output.push_str("then");
            ugly_block_or_whitespace(block, output);
        }

        if let Some(no) = &self.no {
            output.push_str("else");
            ugly_block_or_whitespace(no, output);
        }

        output.push_str("end");
    }
}

impl<S> Ugly for While<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("while");
        ugly_with_padding(&self.condition, output);
        output.push_str("do");
        ugly_block_or_whitespace(&self.block, output);
        output.push_str("end");
    }
}

impl<S> Ugly for Repeat<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("repeat");
        ugly_block_or_whitespace(&self.block, output);
        output.push_str("until");
        ugly_with_prefix_padding(&self.condition, output);
    }
}

impl<S> Ugly for For<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("for ");
        output.push_str(self.name.as_ref());
        output.push_char('=');

        self.start.ugly(output);
        output.push_char(',');
        if let Some(step) = &self.step {
            self.end.ugly(output);
            output.push_char(',');
            ugly_with_suffix_padding(step, output);
        } else {
            ugly_with_suffix_padding(&self.end, output);
        }

        output.push_str("do");
        ugly_block_or_whitespace(&self.block, output);
        output.push_str("end");
    }
}

impl<S> Ugly for ForIn<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str("for ");
        let mut names = self.names.iter();
        output.push_str(names.next().unwrap().as_ref());
        for name in names {
            output.push_char(',');
            output.push_str(name.as_ref());
        }
        output.push_str(" in");

        // TODO: Don't have a `last` variable, if possible.
        let mut values = self.values.iter();
        let mut last = values.next().unwrap();
        ugly_with_prefix_padding(last, output);
        for value in values {
            output.push_char(',');
            value.ugly(output);

            last = value;
        }
        if last.suffix_with_whitespace() {
            output.push_char(' ');
        }

        output.push_str("do");
        ugly_block_or_whitespace(&self.block, output);
        output.push_str("end");
    }
}

impl<S> Ugly for Statement<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        match self {
            Self::Local(local) => {
                local.ugly(output);
            }
            Self::Assign(assign) => {
                assign.ugly(output);
            }
            Self::Function(function) => {
                function.ugly(output);
            }
            Self::LocalFunction(local_function) => {
                local_function.ugly(output);
            }
            Self::Call(call) => {
                call.ugly(output);
            }
            Self::Do(do_) => {
                do_.ugly(output);
            }
            Self::If(if_) => {
                if_.ugly(output);
            }
            Self::Label(label) => {
                label.ugly(output);
            }
            Self::Goto(goto) => {
                goto.ugly(output);
            }
            Self::While(while_) => {
                while_.ugly(output);
            }
            Self::Repeat(repeat) => {
                repeat.ugly(output);
            }
            Self::For(for_) => {
                for_.ugly(output);
            }
            Self::ForIn(for_in) => {
                for_in.ugly(output);
            }
            Self::Break => {
                output.push_str("break");
            }
        }
    }
}

impl<S> Ugly for Field<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        match self {
            Self::Name(name, value) => {
                output.push_str(name.as_ref());
                output.push_char('=');
                value.ugly(output);
            }
            Self::Key(key, value) => {
                output.push_char('[');
                key.ugly(output);
                output.push_str("]=");
                value.ugly(output);
            }
            Self::Item(value) => {
                value.ugly(output);
            }
        }
    }
}

impl Ugly for UnaryOp {
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str(self.symbol());
    }
}

impl<S> Ugly for Unary<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        self.op.ugly(output);
        if self.op.suffix_with_whitespace() && self.value.prefix_with_whitespace() {
            output.push_char(' ');
        }
        self.value.ugly(output);
    }
}

impl Ugly for BinaryOp {
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        output.push_str(self.symbol());
    }
}

impl<S> Ugly for Binary<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        self.left.ugly(output);
        if self.left.suffix_with_whitespace() && self.op.prefix_with_whitespace() {
            output.push_char(' ');
        }
        self.op.ugly(output);
        if self.op.suffix_with_whitespace() && self.right.prefix_with_whitespace() {
            output.push_char(' ');
        }
        self.right.ugly(output);
    }
}

impl<S> Ugly for Expression<S>
where
    S: AsRef<str>,
{
    fn ugly<O>(&self, output: &mut O)
    where
        O: Output,
    {
        match self {
            Self::Nil => {
                output.push_str("nil");
            }
            Self::Boolean(boolean) => {
                output.push_str(if *boolean { "true" } else { "false" });
            }
            Self::Number(number) => {
                output.push_str(number.as_ref());
            }
            Self::String(string) => {
                output.push_char('"');
                output.push_str(&escape_double(string.as_ref()));
                output.push_char('"');
            }
            Self::Table(table) => {
                output.push_char('{');
                let mut fields = table.iter();
                if let Some(first) = fields.next() {
                    first.ugly(output);
                    for field in fields {
                        output.push_char(',');
                        field.ugly(output);
                    }
                }
                output.push_char('}');
            }
            Self::Function(function) => {
                function.ugly(output);
            }
            Self::Unary(unary) => {
                unary.ugly(output);
            }
            Self::Binary(binary) => {
                binary.ugly(output);
            }
            Self::Prefix(prefix) => {
                prefix.ugly(output);
            }
            Self::Ellipsis => {
                output.push_str("...");
            }
        }
    }
}
