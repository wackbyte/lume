use super::*;

macro_rules! assert_ugly {
    (
        $node:expr,
        $expected:literal
        $(,)?
    ) => {{
        let node = $node;
        let expected = $expected;

        let mut buffer = String::new();
        node.ugly(&mut buffer);
        assert_eq!(buffer, expected);
    }};
}

// TODO: Simplify making syntax nodes so I can write more tests.

#[test]
fn emit_block_1() {
    assert_ugly!(
        Block {
            body: vec![Statement::Call(Call {
                prefix: Prefix::Variable(Box::new(Variable::Name("print"))),
                method: None,
                arguments: vec![Expression::String("Hello, world!")],
            })],
            result: None,
        },
        r#"print("Hello, world!")"#,
    );
}

#[test]
fn emit_block_2() {
    assert_ugly!(
        Block {
            body: Vec::new(),
            result: Some(Return {
                values: vec![Expression::Prefix(Prefix::Call(Box::new(Call {
                    prefix: Prefix::Group(Box::new(Group {
                        inner: Expression::Function(Box::new(InlineFunction {
                            body: FunctionBody {
                                parameters: vec!["x"],
                                ellipsis: false,
                                block: Block {
                                    body: Vec::new(),
                                    result: Some(Return {
                                        values: vec![Expression::Prefix(Prefix::Variable(
                                            Box::new(Variable::Name("x"))
                                        ))],
                                    })
                                }
                            }
                        })),
                    })),
                    method: None,
                    arguments: vec![Expression::String("id")],
                })))]
            }),
        },
        r#"return(function(x)return x end)("id")"#,
    );
}

#[test]
fn emit_inline_function_1() {
    assert_ugly!(InlineFunction::<&str>::default(), r#"function()end"#);
}

#[test]
fn emit_inline_function_2() {
    assert_ugly!(
        InlineFunction {
            body: FunctionBody {
                parameters: Vec::new(),
                ellipsis: false,
                block: Block {
                    body: Vec::new(),
                    result: Some(Return {
                        values: vec![Expression::String("function")],
                    })
                }
            }
        },
        r#"function()return"function"end"#,
    );
}

#[test]
fn emit_inline_function_3() {
    assert_ugly!(
        InlineFunction {
            body: FunctionBody {
                parameters: Vec::new(),
                ellipsis: false,
                block: Block {
                    body: Vec::new(),
                    result: Some(Return {
                        values: vec![Expression::Number("5")]
                    })
                }
            }
        },
        r#"function()return 5 end"#,
    );
}
