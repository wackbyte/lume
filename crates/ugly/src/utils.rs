use {
    crate::Ugly,
    lume_ast::*,
    lume_emit::{Output, Whitespace},
};

/// Emit a node with padding if it requires it.
pub fn ugly_with_padding<N, O>(node: &N, output: &mut O)
where
    N: Ugly + Whitespace,
    O: Output,
{
    if node.prefix_with_whitespace() {
        output.push_char(' ');
    }
    node.ugly(output);
    if node.suffix_with_whitespace() {
        output.push_char(' ');
    }
}

/// Emit a node with prefix padding if it requires it.
pub fn ugly_with_prefix_padding<N, O>(node: &N, output: &mut O)
where
    N: Ugly + Whitespace,
    O: Output,
{
    if node.prefix_with_whitespace() {
        output.push_char(' ');
    }
    node.ugly(output);
}

/// Emit a node with suffix padding if it requires it.
pub fn ugly_with_suffix_padding<N, O>(node: &N, output: &mut O)
where
    N: Ugly + Whitespace,
    O: Output,
{
    node.ugly(output);
    if node.suffix_with_whitespace() {
        output.push_char(' ');
    }
}

/// Emit whitespace if a block is empty. Otherwise, emit it with padding.
pub fn ugly_block_or_whitespace<S, O>(block: &Block<S>, output: &mut O)
where
    S: AsRef<str>,
    O: Output,
{
    if block.is_empty() {
        output.push_char(' ');
    } else {
        ugly_with_padding(block, output);
    }
}
